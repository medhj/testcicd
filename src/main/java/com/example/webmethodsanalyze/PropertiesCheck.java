package com.example.webmethodsanalyze;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PropertiesCheck {
    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());
    static Boolean Disable=false;
	public static Boolean StatelessProperties(Node node) {
		
		
		
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("Values")) {
        		
        		 NodeList children = node.getChildNodes();

                 for (int i = 0; i < children.getLength(); i++) {
                 	if(children.item(i).getNodeName().equals("value")&&children.item(i).getAttributes().item(0).getNodeValue().equals("stateless")) {
                 		if(children.item(i).getTextContent().equals("yes")) return true;
                 		
                 	}
                }	
        	}
        }
		return false;
		
	}
	
	
	public static Boolean checkStatelessProperties(Node node, String FileName) {
		
		 if(StatelessProperties(node)) {
        	 logger.info("the flow service "+FileName+ " is using Stateless Property");
         	return true;
         }
         else {
        	 logger.warning("the flow service "+FileName+ " isn't using Stateless Property :  Stateless services do not maintain session state and therefore leave a more light footprint on the IS in terms of memory, os , database…");
        	 return false;
         }
		
	}
	
	public static Boolean checkDisableProperty(Node node, String FileName) {
		DisableProperty(node,FileName);
		return Disable;
	}
	
	
	public static void DisableProperty(Node node, String FileName) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	
	           
        	if(node.getAttributes().getNamedItem("DISABLED")!=null) {
        		if(node.getAttributes().getNamedItem("DISABLED").getNodeValue().equals("true")) {
        			logger.severe("the flow service "+FileName+" contains the disabled step : "+ node.getNodeName());
        			if(!Disable) {
        				Disable=true;
        			}
        		}
        	}
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	checkDisableProperty(children.item(i), FileName);
            }
        }
    }
    
	
	public static Boolean checkCachingProperties(Node node, Node Xmlfile, String FileName) {
		
		
		Boolean Caching=false;
		Boolean Prefetch=false;
		String Cache_Expiry_Time="";
		
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("Values")) {
        		
        		 NodeList children = node.getChildNodes();

                 for (int i = 0; i < children.getLength(); i++) {
                 	if(children.item(i).getNodeName().equals("value")&&children.item(i).getAttributes().item(0).getNodeValue().equals("caching")) {
                 		if(children.item(i).getTextContent().equals("yes")) Caching=true;
                 		//// check pipeline
                 		
                 	}
                 	if(children.item(i).getNodeName().equals("value")&&children.item(i).getAttributes().item(0).getNodeValue().equals("prefetch")) {
                 		if(children.item(i).getTextContent().equals("yes")) Prefetch=true;
                 		
                 	}
                 	if(children.item(i).getNodeName().equals("value")&&children.item(i).getAttributes().item(0).getNodeValue().equals("cache_ttl")) {
                 		Cache_Expiry_Time= children.item(i).getTextContent();
                 		break;
                 		
                 	}
                 	
                 	
                }	
        	}
        }
			logger.info("The flow Service "+FileName+" has the Caching Property \""+Caching+"\" with the Prefetch value \""+Prefetch+"\" and the caching expiry time :"+Cache_Expiry_Time);
			if(Caching) {
				if(!InvokedServicesCheck.checkClearPiplineInvokedFirst(Xmlfile)) {
					logger.severe("Ensure that the cached service starts with pub.flow:clearPipeline to clear any residual pipeline data, preventing unintended data from being cached.");
					return true;
				}
				else {
					logger.warning("you should set an appropriate cache expiry time ("+Cache_Expiry_Time+") to balance between performance gain and memory usage.");
					if(Prefetch) {
						logger.warning("Make sure that the prefetch activation threshold is set appropriately to avoid unnecessary re-executions that can consume additional memory and processing power.");
					}
				}
			}
			
		return false;
		
	}
	
	
	public static Boolean checkAuditProperties(Node Pathfile, String FileName) {
		Map<String,String> map=getAuditProperties(Pathfile);
		logger.info("The Flow Service "+FileName+"has the Properties:/n"
				+ "Enable auditing: "+map.get("Enable auditing")+" /n"
				+ "Include pipline: "+map.get("Include pipline")+" /n"
				+ "Log on"+map.get("Log on")+"");
		
		
		int error=0;
		if(map.get("Enable auditing").equals("Always")) {
			error++;
			 logger.severe("The Flow Service "+FileName+"use the Property Enable audithing with the value <Always> : High performance impact due to continuous auditing. Should be avoided for high-volume services in production");
		}
		if(map.get("Enable auditing").equals("When top-level service only")) {
			logger.warning("The Flow Service "+FileName+" use the Property Enable audithing with the value <When top-level service only> :Moderate performance impact as it limits auditing to top-level services. Appropriate for production when some level of auditing is required without too much overhead.");
		}
		if(map.get("Include pipline").equals("Always")) {
			error++;
			 logger.severe("The Flow Service "+FileName+" use the Property Include pipline with the value <Always> : High performance impact due to inclusion of all pipeline data. Should be avoided in production unless absolutely necessary.");
			 		
		}
		if(map.get("Include pipline").equals("On error only")) {
			logger.warning("The Flow Service "+FileName+" use the Property Include pipline with the value <On error only> : Moderate performance impact as pipeline data is included only when errors occur. Suitable for production as it helps in debugging errors with minimal impact.");
		}
		if(map.get("Log on").equals("Error, success, and start")) {
			error++;
			 logger.severe("The Flow Service "+FileName+" use the Property Log on with the value <Error, success, and start> :  Maximum logging, including all events. High performance impact and may cause excessive logging data.");
		}
		if(map.get("Log on").equals("Error and success")) {
			 logger.severe("The Flow Service "+FileName+" use the Property Log on with the value <Error and success> :  Moderate logging, covering both errors and successful executions. Moderate performance impact and suitable for critical services where success and error information are necessary.");
		}
		if(error>0) return true;
		return false;
	}
	
	
	
public static Map<String,String> getAuditProperties(Node node) {
	Map<String,String> map=new HashMap<String, String>();
	
		
		
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("Values")) {
        		
        		 NodeList children = node.getChildNodes();

                 for (int i = 0; i < children.getLength(); i++) {
                 	if(children.item(i).getNodeName().equals("value")&&children.item(i).getAttributes().getNamedItem("name")!=null&&
                 			children.item(i).getAttributes().getNamedItem("name").getNodeValue().equals("auditoption")) {
                 		if(children.item(i).getTextContent().equals("0")) {
                 			map.put("Enable auditing", "Never");
                 			//System.out.println("Enable auditing: Never");
                 		}
                 		else if(children.item(i).getTextContent().equals("1")) {
                 			map.put("Enable auditing", "Always");
                 			//System.out.println("Enable auditing: Always");

                 		}
                 		else {
                 			map.put("Enable auditing", "When top-level service only");
                 			//System.out.println("Enable auditing: When top-level service only");

                 		}
                 		
                 	}
                 	
                 	if(children.item(i).getNodeName().equals("record")&&children.item(i).getAttributes().getNamedItem("name")!=null&&
                 			children.item(i).getAttributes().getNamedItem("name").getNodeValue().equals("auditsettings")) {
                 		NodeList nodelist=children.item(i).getChildNodes();
                 		String[] Logonvalues=new String[3];
                 		for(int j=0; j<nodelist.getLength();j++) {
                 			if(nodelist.item(j).getNodeName().equals("value")) {
                 				if(nodelist.item(j).getAttributes().getNamedItem("name").getNodeValue().equals("document_data")) {
	                 				map.put("Include pipline", getDocumentdatavalue(nodelist.item(j).getTextContent()));	
	                 				//System.out.println("Include pipline : "+getDocumentdatavalue(nodelist.item(j).getTextContent()));
	                 			}
	                 			if(nodelist.item(j).getAttributes().getNamedItem("name").getNodeValue().equals("startExecution")) {
	                 				Logonvalues[0]=nodelist.item(j).getTextContent();	
	                 			}
	                 			if(nodelist.item(j).getAttributes().getNamedItem("name").getNodeValue().equals("stopExecution")) {
	                 				Logonvalues[1]=nodelist.item(j).getTextContent();                 			
	                 			}
	                 			if(nodelist.item(j).getAttributes().getNamedItem("name").getNodeValue().equals("onError")) {
	                 				Logonvalues[0]=nodelist.item(j).getTextContent();                 			
	                 			}
                 			}
                 			
                 		}	
                 		map.put("Log on",getLogonvalue(Logonvalues));
                 		
                 		
                 		
                 	}
                 	
                }	
        	}
        }
		return map;
		
	}

	public static String getDocumentdatavalue(String value) {
		if(value.equals("0")) return "Never";
		else if(value.equals("1")) return "On error only";
		else return "Always";
	}
	
	public static String getLogonvalue(String[] value) {
		if(value[0].equals("true")&&value[1].equals("true")&&value[2].equals("true")) {
			//System.out.println("Log on : Error, success, and start");
			return "Error, success, and start";
		}
		else if(value[0].equals("false")&&value[1].equals("true")&&value[2].equals("true")) {
			//System.out.println("Log on : Error and success");
			return "Error and success";
			}
		else {
			//System.out.println("Log on : Error");
			return "Error";
		}
	}
	
}
