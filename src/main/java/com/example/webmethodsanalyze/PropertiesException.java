package com.example.webmethodsanalyze;

public class PropertiesException extends Exception {
	public PropertiesException() {
        super();
    }

    
    public PropertiesException(String message) {
        super(message);
    }
}


